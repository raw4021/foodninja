<?php
session_start();
?>
<html>
<head>
<title> You have logged out. </title>
<? include 'header.php'; ?>
<link rel="stylesheet" href="css/main.css" type="text/css" />
</head>
<body>

<?php
// remove all session variables
session_unset(); 

// destroy the session 
session_destroy(); 
?>
<!DOCTYPE html>
<div data-role="page" data-theme='b' id="main">
<div data-role="content">   
<div class='inset'>
	<center>
		<div style="width:85%;">
			<img src="./images/templogo.png" alt="Food Ninja Temp Logo"/>

			<h1> You logged out </h1>
			<a href="http://foodninja.co.nf/index.php" data-ajax="false">Click Here To Log Back In</a>
			
		</div>
	</center>
</div>
</div>
</div>
</body>
</html>