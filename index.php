<? include 'accesscontrol.php'; ?>
<!DOCTYPE HTML>
<html>
<? include 'header.php'; ?>

    <div data-role="page" data-theme='b' id="main">
	<div
	  class="fb-like"
	  data-share="true"
	  data-width="450"
	  data-show-faces="true">
	</div>
		<? include 'menu.php' ?>
	
        <div data-role="header" data-position="fixed" data-tap-toggle="false" data-theme='b'>
            <a href="#mypanel" data-ajax="false"><i class='fa fa-bars'></i></a>
            <h5 id="welcome_div">Welcome, <?echo $uid ?></h5>
        </div>
        
        <div data-role="content"> 
			<ul data-role="listview" data-inset="false" data-icon="false" data-divider-theme="b">
			
				<li>
					<a href="displayresults.php">
						<img src="./images/pref.png">
						<h2><i class='blIcon fa fa-magic'></i><Strong>By Preference</strong></h2> 
					</a>
				</li>
				
				<li>
					<a href="by.php">
					<img src="./images/menu.png">
						<h2><i class='blIcon fa fa-cutlery'></i><Strong>Browse All</strong></h2>
					</a>
				</li>
				
				
			
			</ul>
	        
        </div>

    </div>
    
    </body>
</html>
