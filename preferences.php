<? include 'accesscontrol.php'; ?>

<!DOCTYPE HTML>
<html>
<? include 'header.php'; ?>
    <div data-role="page" data-theme='b'>
	
		<? include 'menu.php' ?>
	
        <div data-role="header" data-position="fixed" data-tap-toggle="false" data-theme='b'>
            <a href="#mypanel" data-ajax="false"><i class='fa fa-bars'></i></a>
            <h3>Preferences: <? echo $uid?> </h3>
        </div>
        
        <div data-role="content">   
            <div data-role="collapsible">
                <h3>Likes</h3>
                <ul data-role="listview">                 
				 
				 <?
					// WHERE the username of the user is equal to $uid
					// AND the userid of the user is equal to userid in the userlikes table
					// AND the ingrid in the userlikes table is equal to the ingrid in the ingredient table
					
					$sql = "SELECT * FROM user, userlikes, ingredient WHERE (user.username = '$uid' AND user.userid = userlikes.userid AND userlikes.ingrid = ingredient.ingrid)";
					$result = mysql_query($sql);
				if (mysql_num_rows($result) > 0) {
					
						// output data of each row
					
					while($row = mysql_fetch_row($result) ) {
						?>
						<li> 
						
						<!-- pass the ingrid & the ingrname to DISH results page -->
						
						<a href="displayIngredientsCategories_results_DISHES.php?ingrid=<?echo $row[5]?>&ingrname=<?echo $row[6]?>">
						
						<!-- echo the ingrname here -->
						
						<font color="black"><Strong><? echo $row[6]?></Strong></font>
						
						</a> 
						
						</li>
						<?
					}
				} else {
					echo "No 'Likes' found.";
				}

				?>				 
				 
                </ul>
            </div>
			<div data-role="collapsible">
                <h3>Dislikes</h3>
                <ul data-role="listview">
                    
				<?
					// WHERE the username of the user is equal to $uid
					// AND the userid of the user is equal to userid in the userdis table
					// AND the ingrid in the userdis table is equal to the ingrid in the ingredient table
					
					$sql = "SELECT * FROM user, userdis, ingredient WHERE (user.username = '$uid' AND user.userid = userdis.userid AND userdis.ingrid = ingredient.ingrid)";
					$result = mysql_query($sql);
				if (mysql_num_rows($result) > 0) {
					
						// output data of each row
					
					while($row = mysql_fetch_row($result) ) {
						?>
						<li> 
						
						<!-- pass the ingrid to this page -->
						
						<a href="displayIngredientsCategories_results_DISHES.php?ingrid=<?echo $row[5]?>&ingrname=<?echo $row[6]?>">
						
						<!-- echo the ingrname here -->
						
						<font color="black"><Strong><? echo $row[6]?></Strong></font>
						
						</a> 
						
						</li>
						<?
					}	
					
				} else {
					echo "No 'Dislikes' found.";
				}

				?>	
					
                </ul>
            </div>
			<div data-role="collapsible">
                <h3>Allergens</h3>
                <ul data-role="listview">
                  
					<?
					
						// similar to the last 2
					
					$sql = "SELECT * FROM user, useraller, ingredient WHERE (user.username = '$uid' AND user.userid = useraller.userid AND useraller.ingrid = ingredient.ingrid)";
					$result = mysql_query($sql);
				if (mysql_num_rows($result) > 0) {
					
						// output data of each row
					
					while($row = mysql_fetch_row($result) ) {
						?>
						<li> 
						
						<!-- pass the ingrid to this page -->
						
						<a href="displayIngredientsCategories_results_DISHES.php?ingrid=<?echo $row[5]?>&ingrname=<? echo $row[6]?>">
						
						<!-- echo the ingrname here -->
						
						<font color="black"><Strong><? echo $row[6]?></Strong></font>
						
						</a> 
						
						</li>
						<?
					}
					
				} else {
					echo "No 'Allergens' found.";
				}

				?>
				  
                </ul>
            </div>
        </div>
		
		<div data-position="fixed" data-tap-toggle="false" data-role="footer" data-tap-toggle="false" data-theme='b'>
			<div data-role="navbar">
				<ul>
					<li><a href="preferencewizard.php">
					<font color = "black"><i class='blIcon fa fa-cogs'></i></font>
					<font color = "black"><Strong>Config</Strong></font></a></li>
					
					<li><a href="displayresults.php">
					<font color = "black"><i class='blIcon fa fa-magic'></i></font>
					<font color = "black"><Strong>Find</Strong></font></a></li>
					
					<li><a href="https://www.facebook.com/mobile/">
					<font color = "black"><i class='blIcon fa fa-facebook-square'></i></font>
					<font color = "black"><Strong>Share!</Strong></font></a></li>
				</ul>
			</div>
		</div>
	        
	        
    </div>
    
    </body>
</html>
