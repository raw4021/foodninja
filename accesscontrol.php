<?php // accesscontrol.php
include_once 'db.php';

session_start();

$uid = isset($_POST['uid']) ? $_POST['uid'] : $_SESSION['uid'];
$pwd = isset($_POST['pwd']) ? $_POST['pwd'] : $_SESSION['pwd'];

if(!isset($uid)) {
?>
<!DOCTYPE html>
<html>
<head>
<title> Please Log In for Access </title>
<? include 'header.php'; ?>
<link rel="stylesheet" href="css/main.css" type="text/css" />
</head>
<body>
<div data-role="page" data-theme='b' id="main">
<div data-role="content">   
<div class='inset'>
	<center>
		<div style="width:85%;">
			<img src="./images/templogo.png" alt="Food Ninja Temp Logo"/>

			<h1> Login Required </h1>

			<a href="http://foodninja.co.nf/signup.php" data-ajax="false">Click Here To Register</a>
				<form method="post" action="<?=$_SERVER['PHP_SELF']?>">
					<li data-role="fieldcontain">
                        <label for="name2b">User ID:</label>
                        <input type="text" name="uid" size="8" id="name2b" value="" data-clear-btn="true" placeholder="example@email.com">
                    </li>
					<li data-role="fieldcontain">
                        <label for="name2b">Password:</label>
                        <input type="password" name="pwd" size="8" id="name2b" value="" data-clear-btn="true" placeholder="">
                    </li>
							<button type="submit" data-inline='true'><i class='lIcon fa fa-check'></i>Log In</button>
							<button type="reset" data-inline='true'><i class='lIcon fa fa-times'></i>Cancel</button>
				</form>
			
		</div>
	</center>
</div>
</div>
</div>
</body>
</html>
<?php
exit;
}

$_SESSION['uid'] = $uid;
$_SESSION['pwd'] = $pwd;

dbConnect("1799664_test");
$sql = "SELECT * FROM `user` S WHERE S.username = '$uid' AND S.password = '$pwd'";
$result = mysql_query($sql);
if (!$result) {
?>
<!DOCTYPE html>
<html>
<head>
<title> Please Log In for Access </title>
<? include 'header.php'; ?>
<link rel="stylesheet" href="css/main.css" type="text/css" />
</head>
<body>
<div data-role="page" data-theme='b' id="main">
<div data-role="content">   
<div class='inset'>
	<center>
		<div style="width:85%;">
			<img src="./images/templogo.png" alt="Food Ninja Temp Logo"/>

			<h1> Login Required </h1>
			<h2> There was an internal problem, Please try again </h2>

			<a href="http://foodninja.co.nf/signup.php" data-ajax="false">Click Here To Register</a>
				<form method="post" action="<?=$_SERVER['PHP_SELF']?>">
					<li data-role="fieldcontain">
                        <label for="name2b">User ID:</label>
                        <input type="text" name="uid" size="8" id="name2b" value="" data-clear-btn="true" placeholder="example@email.com">
                    </li>
					<li data-role="fieldcontain">
                        <label for="name2b">Password:</label>
                        <input type="password" name="pwd" size="8" id="name2b" value="" data-clear-btn="true" placeholder="">
                    </li>
							<button type="submit" data-inline='true'><i class='lIcon fa fa-check'></i>Log In</button>
							<button type="reset" data-inline='true'><i class='lIcon fa fa-times'></i>Cancel</button>
				</form>
			
		</div>
	</center>
</div>
</div>
</div>
</body>
</html>
<?php
exit;
}


if (mysql_num_rows($result) == 0) {
unset($_SESSION['uid']);
unset($_SESSION['pwd']);
?>
<!DOCTYPE html>
<html>
<head>
<title>Access Denied </title>
<? include 'header.php'; ?>
<link rel="stylesheet" href="css/main.css" type="text/css" />
</head>
<body>
<div data-role="page" data-theme='b' id="main">
<div data-role="content">   
<div class='inset'>
	<center>
		<div style="width:85%;">
			<img src="./images/templogo.png" alt="Food Ninja Temp Logo"/>
			<h1> Access Denied </h1>
			<p>Your user ID or password is incorrect, or you are not a
			registered user on this site. <br/>To try logging in again, click
			<a href="<?=$_SERVER['PHP_SELF']?>" data-ajax="false">here</a>.<br/> To register for instant
			access, click <a href="signup.php" data-ajax="false">here</a>.</p>
		</div>
	</center>
</div>
</div>
</div>
</body>
</html>
<?php
exit;
}
$row = mysql_fetch_array($result);
$userid = $row["userid"];
?>