<?php 
// signup.php refence goes to: http://www.sitepoint.com/users-php-sessions-mysql/ 
// passoword requirements reference goes to:http://stackoverflow.com/questions/11873990/create-preg-match-for-password-validation-allowing 
// form generated at http://www.reformedapp.com/#form_main

include 'db.php';
if (!isset($_POST['submitok'])):
// Display the user signup form
?>
<!DOCTYPE html>
<html>
<head>
<!-- jquery -->
 <script src="js/jquery-1.4.4.min.js" type="text/javascript"></script>
 <script src="js/jquery-ui-1.8.7/ui/minified/jquery-ui.min.js" type="text/javascript"></script>

<!-- necessary reformed CSS -->
<!--[if IE]>
    <link rel="stylesheet" type="text/css" href="reformed/css/ie_fieldset_fix.css" />
<![endif]-->
<link rel="stylesheet" href="reformed/css/uniform.aristo.css" type="text/css" />
<link rel="stylesheet" href="reformed/css/ui.reformed.css" type="text/css" />
<link rel="stylesheet" href="reformed-form-blitzer/jquery-ui-1.8.7.custom.css" type="text/css" />
<!-- end necessary reformed CSS -->

<!-- necessary reformed js -->	
<script src="reformed/js/jquery.uniform.min.js" type="text/javascript"></script>
<script src="reformed/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="reformed/js/jquery.ui.reformed.min.js" type="text/javascript"></script>
<!-- end necessary reformed js -->  
<link rel="stylesheet" href="css/main.css" type="text/css" />
</head>
<body>
<script type="text/javascript">
$(function(){ //on doc ready
    //set validation options
    //(this creates range messages from max/min values)
    $.validator.autoCreateRanges = true;
    $.validator.setDefaults({
        highlight: function(input) {
            $(input).addClass("ui-state-highlight");
        },
        unhighlight: function(input) {
            $(input).removeClass("ui-state-highlight");
        },
        errorClass: 'error_msg',
        wrapper : 'dd',
        errorPlacement : function(error, element) {
            error.addClass('ui-state-error');
            error.prepend('<span class="ui-icon ui-icon-alert"></span>');
            error.appendTo(element.closest('dl.ui-helper-clearfix').effect('highlight', {}, 2000));
        }
    });

    //call reformed and the validation library on your form
    $('#signup').reformed().validate();
	$('#signup').reformed({
            styleFileInputs : true, //use the uniform plugin to style file input boxes
            styleRadios : true, //style radios with uniform plugin
            styleCheckboxes : true, //style checkboxes with uniform plugin
            styleSelects : true, //style selects with uniform plugin
            styleButtonsWithUniform : false, //style all form buttons with uniform (false = styled by jquery UI)
            styleDatepicker : true //use jqueryUI datepicker
	}).validate();
});
</script>

<div class="reformed-form">
	<center><img src="./images/templogo.png" alt="Food Ninja Temp Logo"></center>
	<form method="post" name="signup" id="signup" action="signup.php" enctype="multipart/form-data">
		<dl>
			<dt>
				<label for="Email">Email</label>
			</dt>
			<dd><input value="<? echo $_POST['Email']; ?>" type="text" id="Email" class="required  email" name="Email" minlength="1" maxlength="255" /></dd>
		</dl>
		<dl>
			<dt>
				<label for="Password">Password</label>
			</dt>
			<dd><input type="password" id="Password" class="required" name="Password" minlength="8" maxlength="32" /></dd>
		</dl>
		<dl>
			<dt>
				<label for="PasswordCopy">Re-enter Password</label>
			</dt>
			<dd><input type="password" id="PasswordCopy" class="required" name="PasswordCopy" minlength="8" maxlength="32" /></dd>
		</dl>
		<dl>
			<dt>
				<label class="verification_question" for="Verify">Human Verification: What is 2 + 5?</label>
			</dt>
			<dd><input style="width:30px;" type="text" id="Verify" class="human required required" name="Verify" minlength="1" maxlength="255" /></dd>
		</dl>
		<div id="submit_buttons">
			<button type="reset">Reset</button>
			<button type="submit" name="submitok" >Submit</button>
		</div>
		</form>
</div>

</body>
</html>

<?php
else:
// Process signup submission
dbConnect('1799664_test');
if ($_POST['Password'] <> $_POST['PasswordCopy']) {
?>
<!DOCTYPE html>
<html>
<head>
<!-- jquery -->
 <script src="js/jquery-1.4.4.min.js" type="text/javascript"></script>
 <script src="js/jquery-ui-1.8.7/ui/minified/jquery-ui.min.js" type="text/javascript"></script>

<!-- necessary reformed CSS -->
<!--[if IE]>
    <link rel="stylesheet" type="text/css" href="reformed/css/ie_fieldset_fix.css" />
<![endif]-->
<link rel="stylesheet" href="reformed/css/uniform.aristo.css" type="text/css" />
<link rel="stylesheet" href="reformed/css/ui.reformed.css" type="text/css" />
<link rel="stylesheet" href="reformed-form-blitzer/jquery-ui-1.8.7.custom.css" type="text/css" />
<!-- end necessary reformed CSS -->

<!-- necessary reformed js -->	
<script src="reformed/js/jquery.uniform.min.js" type="text/javascript"></script>
<script src="reformed/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="reformed/js/jquery.ui.reformed.min.js" type="text/javascript"></script>
<!-- end necessary reformed js -->  
</head>
<body>
<script type="text/javascript">
$(function(){ //on doc ready
    //set validation options
    //(this creates range messages from max/min values)
    $.validator.autoCreateRanges = true;
    $.validator.setDefaults({
        highlight: function(input) {
            $(input).addClass("ui-state-highlight");
        },
        unhighlight: function(input) {
            $(input).removeClass("ui-state-highlight");
        },
        errorClass: 'error_msg',
        wrapper : 'dd',
        errorPlacement : function(error, element) {
            error.addClass('ui-state-error');
            error.prepend('<span class="ui-icon ui-icon-alert"></span>');
            error.appendTo(element.closest('dl.ui-helper-clearfix').effect('highlight', {}, 2000));
        }
    });

    //call reformed and the validation library on your form
    $('#signup').reformed().validate();
	$('#signup').reformed({
            styleFileInputs : true, //use the uniform plugin to style file input boxes
            styleRadios : true, //style radios with uniform plugin
            styleCheckboxes : true, //style checkboxes with uniform plugin
            styleSelects : true, //style selects with uniform plugin
            styleButtonsWithUniform : false, //style all form buttons with uniform (false = styled by jquery UI)
            styleDatepicker : true //use jqueryUI datepicker
	}).validate();
});
</script>

<div class="reformed-form">
	<center>
		<p>Your passwords do not match.<br>
		Please make sure you retype your password correctly.<br>
		<img src="./images/templogo.png" alt="Food Ninja Temp Logo">
	</center>
	<form method="post" name="signup" id="signup" action="signup.php" enctype="multipart/form-data">
		<dl>
			<dt>
				<label for="Email">Email</label>
			</dt>
			<dd><input value="<? echo $_POST['Email']; ?>" type="text" id="Email" class="required  email" name="Email" minlength="1" maxlength="255" /></dd>
		</dl>
		<dl>
			<dt>
				<label for="Password">Password</label>
			</dt>
			<dd><input type="password" id="Password" class="required" name="Password" minlength="8" maxlength="32" /></dd>
		</dl>
		<dl>
			<dt>
				<label for="PasswordCopy">Re-enter Password</label>
			</dt>
			<dd><input type="password" id="PasswordCopy" class="required" name="PasswordCopy" minlength="8" maxlength="32" /></dd>
		</dl>
		<dl>
			<dt>
				<label class="verification_question" for="Verify">Human Verification: What is 2 + 5?</label>
			</dt>
			<dd><input style="width:30px;" type="text" id="Verify" class="human required required" name="Verify" minlength="1" maxlength="255" /></dd>
		</dl>
		<div id="submit_buttons">
			<button type="reset">Reset</button>
			<button type="submit" name="submitok" >Submit</button>
		</div>
		</form>
</div>

</body>
</html>
<?
exit;
}
if(!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,12}$/', $_POST['Password'])) {
?>
<!DOCTYPE html>
<html>
<head>
<!-- jquery -->
 <script src="js/jquery-1.4.4.min.js" type="text/javascript"></script>
 <script src="js/jquery-ui-1.8.7/ui/minified/jquery-ui.min.js" type="text/javascript"></script>

<!-- necessary reformed CSS -->
<!--[if IE]>
    <link rel="stylesheet" type="text/css" href="reformed/css/ie_fieldset_fix.css" />
<![endif]-->
<link rel="stylesheet" href="reformed/css/uniform.aristo.css" type="text/css" />
<link rel="stylesheet" href="reformed/css/ui.reformed.css" type="text/css" />
<link rel="stylesheet" href="reformed-form-blitzer/jquery-ui-1.8.7.custom.css" type="text/css" />
<!-- end necessary reformed CSS -->

<!-- necessary reformed js -->	
<script src="reformed/js/jquery.uniform.min.js" type="text/javascript"></script>
<script src="reformed/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="reformed/js/jquery.ui.reformed.min.js" type="text/javascript"></script>
<!-- end necessary reformed js -->  
</head>
<body>
<script type="text/javascript">
$(function(){ //on doc ready
    //set validation options
    //(this creates range messages from max/min values)
    $.validator.autoCreateRanges = true;
    $.validator.setDefaults({
        highlight: function(input) {
            $(input).addClass("ui-state-highlight");
        },
        unhighlight: function(input) {
            $(input).removeClass("ui-state-highlight");
        },
        errorClass: 'error_msg',
        wrapper : 'dd',
        errorPlacement : function(error, element) {
            error.addClass('ui-state-error');
            error.prepend('<span class="ui-icon ui-icon-alert"></span>');
            error.appendTo(element.closest('dl.ui-helper-clearfix').effect('highlight', {}, 2000));
        }
    });

    //call reformed and the validation library on your form
    $('#signup').reformed().validate();
	$('#signup').reformed({
            styleFileInputs : true, //use the uniform plugin to style file input boxes
            styleRadios : true, //style radios with uniform plugin
            styleCheckboxes : true, //style checkboxes with uniform plugin
            styleSelects : true, //style selects with uniform plugin
            styleButtonsWithUniform : false, //style all form buttons with uniform (false = styled by jquery UI)
            styleDatepicker : true //use jqueryUI datepicker
	}).validate();
});
</script>

<div class="reformed-form">
		<center>
			<img src="./images/templogo.png" alt="Food Ninja Temp Logo">
			<p>Your password does not meet the minimum requirements.<br>
			Please make sure you password meets the following requirements:<br>
			<ul>
			<li>Must have at least one number</li>
			<li>Must have at least one letter</li>
			<li>Must have to be 8-12 characters</li>
			<li>You may only use these symbols: !@#$%</li>
			</ul>
		</center>
	<form method="post" name="signup" id="signup" action="signup.php" enctype="multipart/form-data">
		<dl>
			<dt>
				<label for="Email">Email</label>
			</dt>
			<dd><input value="<? echo $_POST['Email']; ?>" type="text" id="Email" class="required  email" name="Email" minlength="1" maxlength="255" /></dd>
		</dl>
		<dl>
			<dt>
				<label for="Password">Password</label>
			</dt>
			<dd><input type="password" id="Password" class="required" name="Password" minlength="8" maxlength="32" /></dd>
		</dl>
		<dl>
			<dt>
				<label for="PasswordCopy">Re-enter Password</label>
			</dt>
			<dd><input type="password" id="PasswordCopy" class="required" name="PasswordCopy" minlength="8" maxlength="32" /></dd>
		</dl>
		<dl>
			<dt>
				<label class="verification_question" for="Verify">Human Verification: What is 2 + 5?</label>
			</dt>
			<dd><input style="width:30px;" type="text" id="Verify" class="human required required" name="Verify" minlength="1" maxlength="255" /></dd>
		</dl>
		<div id="submit_buttons">
			<button type="reset">Reset</button>
			<button type="submit" name="submitok" >Submit</button>
		</div>
		</form>
</div>

</body>
</html>
<?
exit;
}
// Check for existing user with the new id
$sql = "SELECT COUNT(*) FROM `user` S WHERE S.username=\"$_POST[Email]\"";
$result = mysql_query($sql);
if (!$result) {
?>
<!DOCTYPE html>
<html>
<head>
<!-- jquery -->
 <script src="js/jquery-1.4.4.min.js" type="text/javascript"></script>
 <script src="js/jquery-ui-1.8.7/ui/minified/jquery-ui.min.js" type="text/javascript"></script>

<!-- necessary reformed CSS -->
<!--[if IE]>
    <link rel="stylesheet" type="text/css" href="reformed/css/ie_fieldset_fix.css" />
<![endif]-->
<link rel="stylesheet" href="reformed/css/uniform.aristo.css" type="text/css" />
<link rel="stylesheet" href="reformed/css/ui.reformed.css" type="text/css" />
<link rel="stylesheet" href="reformed-form-blitzer/jquery-ui-1.8.7.custom.css" type="text/css" />
<!-- end necessary reformed CSS -->

<!-- necessary reformed js -->	
<script src="reformed/js/jquery.uniform.min.js" type="text/javascript"></script>
<script src="reformed/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="reformed/js/jquery.ui.reformed.min.js" type="text/javascript"></script>
<!-- end necessary reformed js -->  
</head>
<body>
<script type="text/javascript">
$(function(){ //on doc ready
    //set validation options
    //(this creates range messages from max/min values)
    $.validator.autoCreateRanges = true;
    $.validator.setDefaults({
        highlight: function(input) {
            $(input).addClass("ui-state-highlight");
        },
        unhighlight: function(input) {
            $(input).removeClass("ui-state-highlight");
        },
        errorClass: 'error_msg',
        wrapper : 'dd',
        errorPlacement : function(error, element) {
            error.addClass('ui-state-error');
            error.prepend('<span class="ui-icon ui-icon-alert"></span>');
            error.appendTo(element.closest('dl.ui-helper-clearfix').effect('highlight', {}, 2000));
        }
    });

    //call reformed and the validation library on your form
    $('#signup').reformed().validate();
	$('#signup').reformed({
            styleFileInputs : true, //use the uniform plugin to style file input boxes
            styleRadios : true, //style radios with uniform plugin
            styleCheckboxes : true, //style checkboxes with uniform plugin
            styleSelects : true, //style selects with uniform plugin
            styleButtonsWithUniform : false, //style all form buttons with uniform (false = styled by jquery UI)
            styleDatepicker : true //use jqueryUI datepicker
	}).validate();
});
</script>

<div class="reformed-form">
	<center>
		<img src="./images/templogo.png" alt="Food Ninja Temp Logo">
		<p>A database error occurred in processing your submission.<br>
		If this error persists, please contact help@foodninja.co.nf<br>
	</center>
	<form method="post" name="signup" id="signup" action="signup.php" enctype="multipart/form-data">
		<dl>
			<dt>
				<label for="Email">Email</label>
			</dt>
			<dd><input value="<? echo $_POST['Email']; ?>" type="text" id="Email" class="required  email" name="Email" minlength="1" maxlength="255" /></dd>
		</dl>
		<dl>
			<dt>
				<label for="Password">Password</label>
			</dt>
			<dd><input type="password" id="Password" class="required" name="Password" minlength="8" maxlength="32" /></dd>
		</dl>
		<dl>
			<dt>
				<label for="PasswordCopy">Re-enter Password</label>
			</dt>
			<dd><input type="password" id="PasswordCopy" class="required" name="PasswordCopy" minlength="8" maxlength="32" /></dd>
		</dl>
		<dl>
			<dt>
				<label class="verification_question" for="Verify">Human Verification: What is 2 + 5?</label>
			</dt>
			<dd><input style="width:30px;" type="text" id="Verify" class="human required required" name="Verify" minlength="1" maxlength="255" /></dd>
		</dl>
		<div id="submit_buttons">
			<button type="reset">Reset</button>
			<button type="submit" name="submitok" >Submit</button>
		</div>
		</form>
</div>

</body>
</html>
<?
exit;
}
$sql = "INSERT INTO `user`(`username`,`password`) VALUES ('$_POST[Email]','$_POST[Password]')";

if (!mysql_query($sql)){
?>
<!DOCTYPE html>
<html>
<head>
<!-- jquery -->
 <script src="js/jquery-1.4.4.min.js" type="text/javascript"></script>
 <script src="js/jquery-ui-1.8.7/ui/minified/jquery-ui.min.js" type="text/javascript"></script>

<!-- necessary reformed CSS -->
<!--[if IE]>
    <link rel="stylesheet" type="text/css" href="reformed/css/ie_fieldset_fix.css" />
<![endif]-->
<link rel="stylesheet" href="reformed/css/uniform.aristo.css" type="text/css" />
<link rel="stylesheet" href="reformed/css/ui.reformed.css" type="text/css" />
<link rel="stylesheet" href="reformed-form-blitzer/jquery-ui-1.8.7.custom.css" type="text/css" />
<!-- end necessary reformed CSS -->

<!-- necessary reformed js -->	
<script src="reformed/js/jquery.uniform.min.js" type="text/javascript"></script>
<script src="reformed/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="reformed/js/jquery.ui.reformed.min.js" type="text/javascript"></script>
<!-- end necessary reformed js -->  
</head>
<body>
<script type="text/javascript">
$(function(){ //on doc ready
    //set validation options
    //(this creates range messages from max/min values)
    $.validator.autoCreateRanges = true;
    $.validator.setDefaults({
        highlight: function(input) {
            $(input).addClass("ui-state-highlight");
        },
        unhighlight: function(input) {
            $(input).removeClass("ui-state-highlight");
        },
        errorClass: 'error_msg',
        wrapper : 'dd',
        errorPlacement : function(error, element) {
            error.addClass('ui-state-error');
            error.prepend('<span class="ui-icon ui-icon-alert"></span>');
            error.appendTo(element.closest('dl.ui-helper-clearfix').effect('highlight', {}, 2000));
        }
    });

    //call reformed and the validation library on your form
    $('#signup').reformed().validate();
	$('#signup').reformed({
            styleFileInputs : true, //use the uniform plugin to style file input boxes
            styleRadios : true, //style radios with uniform plugin
            styleCheckboxes : true, //style checkboxes with uniform plugin
            styleSelects : true, //style selects with uniform plugin
            styleButtonsWithUniform : false, //style all form buttons with uniform (false = styled by jquery UI)
            styleDatepicker : true //use jqueryUI datepicker
	}).validate();
});
</script>

<div class="reformed-form">
	<center>
		<img src="./images/templogo.png" alt="Food Ninja Temp Logo">
		<p>A database error occurred in processing your submission.<br>
		If this error persists, please contact help@foodninja.co.nf<br>
	</center>
	<form method="post" name="signup" id="signup" action="signup.php" enctype="multipart/form-data">
		<dl>
			<dt>
				<label for="Email">Email</label>
			</dt>
			<dd><input value="<? echo $_POST['Email']; ?>" type="text" id="Email" class="required  email" name="Email" minlength="1" maxlength="255" /></dd>
		</dl>
		<dl>
			<dt>
				<label for="Password">Password</label>
			</dt>
			<dd><input type="password" id="Password" class="required" name="Password" minlength="8" maxlength="32" /></dd>
		</dl>
		<dl>
			<dt>
				<label for="PasswordCopy">Re-enter Password</label>
			</dt>
			<dd><input type="password" id="PasswordCopy" class="required" name="PasswordCopy" minlength="8" maxlength="32" /></dd>
		</dl>
		<dl>
			<dt>
				<label class="verification_question" for="Verify">Human Verification: What is 2 + 5?</label>
			</dt>
			<dd><input style="width:30px;" type="text" id="Verify" class="human required required" name="Verify" minlength="1" maxlength="255" /></dd>
		</dl>
		<div id="submit_buttons">
			<button type="reset">Reset</button>
			<button type="submit" name="submitok" >Submit</button>
		</div>
		</form>
</div>

</body>
</html>
<?
exit;
}
?>
<!DOCTYPE html>
<html>
<head>
<!-- jquery -->
 <script src="js/jquery-1.4.4.min.js" type="text/javascript"></script>
 <script src="js/jquery-ui-1.8.7/ui/minified/jquery-ui.min.js" type="text/javascript"></script>

<!-- necessary reformed CSS -->
<!--[if IE]>
    <link rel="stylesheet" type="text/css" href="reformed/css/ie_fieldset_fix.css" />
<![endif]-->
<link rel="stylesheet" href="reformed/css/uniform.aristo.css" type="text/css" />
<link rel="stylesheet" href="reformed/css/ui.reformed.css" type="text/css" />
<link rel="stylesheet" href="reformed-form-blitzer/jquery-ui-1.8.7.custom.css" type="text/css" />
<!-- end necessary reformed CSS -->

<!-- necessary reformed js -->	
<script src="reformed/js/jquery.uniform.min.js" type="text/javascript"></script>
<script src="reformed/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="reformed/js/jquery.ui.reformed.min.js" type="text/javascript"></script>
<!-- end necessary reformed js -->  
</head>
<p><strong>User registration successful!</strong></p>
<p><a href="index.php" data-ajax="false">here</a> to return to the login
page, and enter your new personal userid and password.</p>
</div>

</body>
</html>
<?php
endif;
?>