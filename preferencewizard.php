<? include 'accesscontrol.php'; ?>
<!DOCTYPE HTML>
<html>
<? include 'header.php'; ?>
    <div data-role="page" data-theme='b'>
	
		<? include 'menu.php' ?>
	
        <div data-role="header" data-position="fixed" data-tap-toggle="false" data-theme='b'>
            <a href="#mypanel" data-ajax="false"><i class='fa fa-bars'></i></a>
            <h5>Preference wizard</h5>
        </div>
        
        <div data-role="content">
<script>
function goBack() {
    window.history.back()
}
</script>
            <form class="form-container" method="post" name="create" id="create" action="pre_wiz_processor.php" enctype="multipart/form-data" data-ajax="false">
                <ul data-role="listview" data-inset="false">
        
                    <li data-role="fieldcontain">
                        <fieldset data-role="controlgroup">
                            <legend>Likes</legend>
							 <?
							 //global queries for deselecting from database:
							$notInDislikes = "ingredient.ingrid NOT IN(SELECT ingredient.ingrid FROM user, userdis, ingredient WHERE (user.username = '$uid' AND user.userid = userdis.userid AND userdis.ingrid = ingredient.ingrid))";
							$notInLikes = "ingredient.ingrid NOT IN(SELECT ingredient.ingrid FROM user, userlikes, ingredient WHERE (user.username = '$uid' AND user.userid = userlikes.userid AND userlikes.ingrid = ingredient.ingrid))";
							$notInAllergies = "ingredient.ingrid NOT IN(SELECT ingredient.ingrid FROM user, useraller, ingredient WHERE (user.username = '$uid' AND user.userid = useraller.userid AND useraller.ingrid = ingredient.ingrid))";
							 
							// WHERE the username of the user is equal to $uid
							// AND the userid of the user is equal to userid in the userlikes table
							// AND the ingrid in the userlikes table is equal to the ingrid in the ingredient table
							
							$sql = "SELECT * FROM user, userlikes, ingredient WHERE (user.username = '$uid' AND user.userid = userlikes.userid AND userlikes.ingrid = ingredient.ingrid)";
							$result = mysql_query($sql);
							if (mysql_num_rows($result) > 0) {
							
								// output data of each row
							
								while($row = mysql_fetch_array($result) ) {
							?>
                            <input type="checkbox" name="checkbox-like-<? echo $row["ingrid"];?>" id="checkbox-like-<? echo $row["ingrid"];?>" checked="">
                            <label for="checkbox-like-<? echo $row["ingrid"];?>"><? echo $row["ingrname"];?></label>
							<?
								}
							}
							// WHERE the username of the user is equal to $uid
							// AND the userid of the user is equal to userid in the userlikes table
							// AND the ingrid in the userlikes table is equal to the ingrid in the ingredient table
							$sql = "SELECT * FROM ingredient WHERE $notInLikes AND $notInDislikes AND $notInAllergies";
							$result = mysql_query($sql);
							if (mysql_num_rows($result) > 0) {
							
								// output data of each row
							
								while($row = mysql_fetch_array($result) ) {
							?>
                            <input type="checkbox" name="checkbox-like-<? echo $row["ingrid"];?>" id="checkbox-like-<? echo $row["ingrid"];?>" >
                            <label for="checkbox-like-<? echo $row["ingrid"];?>"><? echo $row["ingrname"];?></label>
							<?
								}
							}
							?>
                        </fieldset>
                    </li>
					<li data-role="fieldcontain">
                        <fieldset data-role="controlgroup">
                            <legend>Dislikes</legend>
							 <?
							// WHERE the username of the user is equal to $uid
							// AND the userid of the user is equal to userid in the userlikes table
							// AND the ingrid in the userlikes table is equal to the ingrid in the ingredient table
							
							$sql = "SELECT * FROM user, userdis, ingredient WHERE (user.username = '$uid' AND user.userid = userdis.userid AND userdis.ingrid = ingredient.ingrid)";
							$result = mysql_query($sql);
							if (mysql_num_rows($result) > 0) {
							
								// output data of each row
							
								while($row = mysql_fetch_array($result) ) {
							?>
                            <input type="checkbox" name="checkbox-dislike-<? echo $row["ingrid"];?>" id="checkbox-like-<? echo $row["ingrid"];?>" checked="">
                            <label for="checkbox-like-<? echo $row["ingrid"];?>"><? echo $row["ingrname"];?></label>
							<?
								}
							}
							// WHERE the username of the user is equal to $uid
							// AND the userid of the user is equal to userid in the userlikes table
							// AND the ingrid in the userlikes table is equal to the ingrid in the ingredient table
							$sql = "SELECT * FROM ingredient WHERE $notInLikes AND $notInDislikes AND $notInAllergies";
							$result = mysql_query($sql);
							if (mysql_num_rows($result) > 0) {
							
								// output data of each row
							
								while($row = mysql_fetch_array($result) ) {
							?>
                            <input type="checkbox" name="checkbox-dislike-<? echo $row["ingrid"];?>" id="checkbox-like-<? echo $row["ingrid"];?>" >
                            <label for="checkbox-like-<? echo $row["ingrid"];?>"><? echo $row["ingrname"];?></label>
							<?
								}
							}
							?>
                        </fieldset>
                    </li>
					<li data-role="fieldcontain">
                        <fieldset data-role="controlgroup">
                            <legend>Allergies</legend>
							 <?
							// WHERE the username of the user is equal to $uid
							// AND the userid of the user is equal to userid in the userlikes table
							// AND the ingrid in the userlikes table is equal to the ingrid in the ingredient table
							
							$sql = "SELECT * FROM user, useraller, ingredient WHERE (user.username = '$uid' AND user.userid = useraller.userid AND useraller.ingrid = ingredient.ingrid)";
							$result = mysql_query($sql);
							if (mysql_num_rows($result) > 0) {
							
								// output data of each row
							
								while($row = mysql_fetch_array($result) ) {
							?>
                            <input type="checkbox" name="checkbox-allergies-<? echo $row["ingrid"];?>" id="checkbox-like-<? echo $row["ingrid"];?>" checked="">
                            <label for="checkbox-like-<? echo $row["ingrid"];?>"><? echo $row["ingrname"];?></label>
							<?
								}
							}
							// WHERE the username of the user is equal to $uid
							// AND the userid of the user is equal to userid in the userlikes table
							// AND the ingrid in the userlikes table is equal to the ingrid in the ingredient table
							
							$sql = "SELECT * FROM ingredient WHERE $notInLikes AND $notInDislikes AND $notInAllergies";
							$result = mysql_query($sql);
							if (mysql_num_rows($result) > 0) {
							
								// output data of each row
							
								while($row = mysql_fetch_array($result) ) {
							?>
                            <input type="checkbox" name="checkbox-allergies-<? echo $row["ingrid"];?>" id="checkbox-like-<? echo $row["ingrid"];?>">
                            <label for="checkbox-like-<? echo $row["ingrid"];?>"><? echo $row["ingrname"];?></label>
							<?
								}
							}
							?>
                        </fieldset>
                    </li>          
                    <li>
							<button type="submit" data-inline='true' ><i class='lIcon fa fa-check'></i>Submit</button>
							<button type="reset" data-inline='true' onclick="goBack()"><i class='lIcon fa fa-times'></i>Cancel</button>
                    </li>
                </ul>
            </form>
        </div>

    </div>
    
    </body>
</html>
