<? include 'accesscontrol.php'; ?>
<!DOCTYPE HTML>
<html>
<? include 'header.php'; ?>
    <div data-role="page" data-theme='b'>


		<? include 'menu.php' ?>
	
        <div data-role="header" data-position="fixed" data-tap-toggle="false" data-theme='b'>
            <a href="#mypanel" data-ajax="false"><i class='fa fa-bars'></i></a>
            <h5>Dish Name</h5>
        </div>
		
		<div data-role="content">   
			<ul data-nativedroid-plugin='cards'>
						<li data-cards-type='text'>
							<ul class='detail'>
								<center>
									<h1>Carne Asada</h1>
									<h2>Mexican</h2>
									<img src="./images/dish/carne-asada.png" alt="Mexican" height="100" width="275">
								</center>
							</ul>
						</li>
				<li data-cards-type='text'>
					<h1>Description:</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nec sapien eu lorem condimentum mollis vel vel mauris. Proin dolor libero, ultricies quis semper eu, bibendum nec quam. Sed pellentesque, nunc vitae ornare tempus, turpis nisl aliquam nisl, vel faucibus neque magna eu odio.</p>
				</li>
				<li data-cards-type='publictransport'>
					<h1>List of ingredients</h1>
					<table>
						<tr>
							<td><p align="left">Ingredient item 1</p></td>
						</tr>
						<tr>
							<td><p align="left">Ingredient item 2</p></td>
						</tr>
						<tr>
							<td><p align="left">Ingredient item 3</p></td>
						</tr>
						<tr>
							<td><p align="left">Ingredient item 4</p></td>
						</tr>
						<tr>
							<td><p align="left">Ingredient item 5</p></td>
						</tr>
						<tr>
							<td ><p align="left"><i class='fa fa-warning'></i> Ingredient item allergic 6</p></td>
						</tr>
						<tr>
							<td><p align="left">Ingredient item 7</p></td>
						</tr>
						
					</table>
					<a href='displayresults.php'><i class='fa fa-file-text-o'></i>Return to results</a>
					<a href='restaurant.php'><i class='fa fa-file-text-o'></i>See Restaurant</a>
				</li>
			</ul> 
        </div>
	</div>
    <script src="js/nativedroid.script.js"></script>
    </body>
</html>