<? include 'accesscontrol.php'; ?>
<!DOCTYPE HTML>
<html>
<? include 'header.php'; ?>
    <div data-role="page" data-theme='b'>
	
		<? include 'menu.php' ?>
	
        <div data-role="header" data-position="fixed" data-tap-toggle="false" data-theme='b'>
            <a href="#mypanel" data-ajax="false"><i class='fa fa-bars'></i></a>
            <h5>Help and FAQ</h5>
        </div>
		
		<div data-role="content">   

	    <div data-role="collapsible" data-content-theme="b">
            <h4>Example question number 1?</h4>
            <ul data-role="listview">
                <li><p>Answer</p></li>
            </ul>
        </div>
		<div data-role="collapsible" data-content-theme="b">
            <h4>Example question number 2?</h4>
            <ul data-role="listview">
                <li><p>Answer</p></li>
            </ul>
        </div>
		<div data-role="collapsible" data-content-theme="b">
            <h4>Example question number 3?</h4>
            <ul data-role="listview">
                <li><p>Answer</p></li>
            </ul>
        </div>
		<div data-role="collapsible" data-content-theme="b">
            <h4>Example question number 4?</h4>
            <ul data-role="listview">
                <li><p>Answer</p></li>
            </ul>
        </div>
		<div data-role="collapsible" data-content-theme="b">
            <h4>Example question number 5?</h4>
            <ul data-role="listview">
                <li><p>Answer</p></li>
            </ul>
        </div>
		<div data-role="collapsible" data-content-theme="b">
            <h4>Example question number 6?</h4>
            <ul data-role="listview">
                <li><p>Answer</p></li>
            </ul>
        </div>
		<div data-role="collapsible" data-content-theme="b">
            <h4>Contact Us</h4>
            <ul data-role="listview">
                <li><p>mobile.solutions.enterprises@gmail.com</p></li>
            </ul>
        </div>

	    <div data-role="collapsible" data-content-theme="b">
            <h4>Upgrade to provider</h4>
			<div class='inset'>	
					<a href="#popupDialog" data-rel="popup" data-position-to="window" data-role="button" data-inline="false" data-transition="pop">
				<div class='message info'>
					<i class='fa fa-envelope-o'></i>
					<center><strong><h5>Request Approval</strong></Center>

				</div>
			</a>
			</div>
        </div>
	    								<div data-role="popup" id="popupDialog" data-theme="b">
									<div data-role="header" data-theme="b">
										<h1>Request Received</h1>
									</div>
										<div data-role="content" data-theme="b">
											<p>Please wait to be contacted by one of our administrators</p>
											<div class='showastabs center nobg'>
													<center><a href="dialog/index.html" data-rel="back" data-role="button" data-inline="true"><i class='lIcon fa fa-check'></i>Okay</a></center>
											</div>
									   
										</div>
								</div>
	        
        </div>

    </div>
    
    </body>
</html>
