-- phpMyAdmin SQL Dump
-- version 3.4.3.1
-- http://www.phpmyadmin.net
--
-- Host: fdb13.biz.nf
-- Generation Time: Apr 21, 2015 at 04:30 PM
-- Server version: 5.5.38
-- PHP Version: 5.3.27

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `1799664_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `allergen`
--

CREATE TABLE IF NOT EXISTS `allergen` (
  `allerid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `allergen_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`allerid`),
  UNIQUE KEY `allerid` (`allerid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `allergen`
--

INSERT INTO `allergen` (`allerid`, `allergen_name`) VALUES
(1, 'aspartame'),
(2, 'corn'),
(3, 'egg'),
(4, 'fish'),
(5, 'shellfish'),
(6, 'garlic'),
(7, 'dairy'),
(8, 'MSG'),
(9, 'soybeans'),
(10, 'sulfites'),
(11, 'tartrazine'),
(12, 'nuts'),
(13, 'wheat'),
(14, 'hydrolized protein');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `catid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`catid`,`category_name`),
  UNIQUE KEY `catid` (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`catid`, `category_name`) VALUES
(1, 'mexican'),
(2, 'protein'),
(3, 'burger'),
(4, 'vegetarian'),
(5, 'salad'),
(6, 'breakfast'),
(7, 'snack'),
(8, 'sandwich'),
(9, 'seafood'),
(10, 'asian');

-- --------------------------------------------------------

--
-- Table structure for table `catofingre`
--

CREATE TABLE IF NOT EXISTS `catofingre` (
  `ingrcatid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `catname` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`ingrcatid`,`catname`),
  UNIQUE KEY `ingrcatid` (`ingrcatid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `catofingre`
--

INSERT INTO `catofingre` (`ingrcatid`, `catname`) VALUES
(1, 'breads'),
(2, 'dressings'),
(3, 'cheeses'),
(4, 'produce'),
(5, 'meat'),
(6, 'spices'),
(7, 'eggs'),
(8, 'fried'),
(9, 'Other'),
(10, 'Nuts'),
(11, 'Seafood');

-- --------------------------------------------------------

--
-- Table structure for table `food`
--

CREATE TABLE IF NOT EXISTS `food` (
  `foodname` varchar(100) DEFAULT NULL,
  `foodid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `price` decimal(20,10) DEFAULT NULL,
  PRIMARY KEY (`foodid`),
  UNIQUE KEY `foodid` (`foodid`),
  UNIQUE KEY `foodid_2` (`foodid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `food`
--

INSERT INTO `food` (`foodname`, `foodid`, `price`) VALUES
('Guacamole Chicken Burrito', 1, 5.7900000000),
('Den Burger', 2, 5.2900000000),
('Veggie Fajita Breakfast Burrito', 3, 4.9900000000),
('Mozzarella Sticks 5pc', 4, 3.9900000000),
('French Fries', 5, 1.7900000000),
('Original Grand Slam', 11, 5.9900000000),
('Grilled Chicken Panini', 10, 5.7900000000),
('Prime Rib Cheesesteak', 9, 6.2900000000),
('Chipotle Prime Rib Breakfast Burrito', 8, 6.4900000000),
('Prime Rib Burrito', 7, 6.2900000000),
('Mozzarella Sticks 8pc.', 6, 4.9900000000),
('Mushroom Swiss Omelette', 14, 5.4900000000),
('Ham and Cheddar Omelette', 13, 5.4900000000),
('Grand Slamwich', 12, 5.6900000000),
('Bacon avocado omelette', 15, 5.4900000000),
('Texas Rib and Egg Sandwich', 16, 5.9900000000),
('Veggie Mash Up Burrito', 17, 5.6900000000),
('Cali jack chicken burger', 18, 5.4900000000),
('Double Den Burger', 19, 6.8900000000),
('Bacon Cheeseburger', 20, 6.6900000000),
('Mushroom Swiss Burger', 21, 5.7900000000),
('Tomahawk Burger', 22, 6.6900000000),
('Southwestern chicken salad', 23, 5.9900000000),
('Pecan cranberry chicken salad', 24, 5.9900000000),
('Bacon Ranch Chicken Salad Sandwich', 25, 5.7900000000),
('Veggie mash up burger', 26, 4.9900000000),
('Chicken strips, 3pc.', 27, 4.4900000000),
('Chicken strips, 5pc.', 28, 5.9900000000),
('Baja Shrimp Cobb', 29, 9.4900000000),
('Derby Chicken Cobb', 30, 9.4900000000),
('Ginger Glazed Salmon', 31, 9.4900000000),
('Turkey Chop', 32, 9.4900000000),
('Wild Alaskan Salmon', 33, 9.4900000000),
('Sophie''s Strawberry Chicken', 34, 9.4900000000),
('Chinese Chopstick', 35, 8.4900000000),
('Apple and Pecan', 36, 8.4900000000),
('Thai Crunch', 37, 8.4900000000),
('Chipotle BBQ Chicken', 38, 8.4900000000),
('Buffalo Chicken Caesar', 39, 8.4900000000),
('Chicken Caesar', 40, 8.4900000000),
('Balsamic Mandarin Orange & Bleu Cheese', 41, 7.4900000000),
('Greekalicious', 42, 7.4900000000),
('Spicy Veggie', 43, 7.4900000000),
('Caesar', 44, 7.4900000000);

-- --------------------------------------------------------

--
-- Table structure for table `foodaller`
--

CREATE TABLE IF NOT EXISTS `foodaller` (
  `foodid` bigint(20) NOT NULL DEFAULT '0',
  `allerid` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`foodid`,`allerid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `foodcat`
--

CREATE TABLE IF NOT EXISTS `foodcat` (
  `foodid` bigint(20) NOT NULL DEFAULT '0',
  `catid` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`foodid`,`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `foodcat`
--

INSERT INTO `foodcat` (`foodid`, `catid`) VALUES
(1, 1),
(1, 2),
(2, 2),
(2, 3),
(2, 8),
(3, 1),
(3, 2),
(3, 4),
(3, 6),
(5, 7),
(6, 4),
(6, 7),
(7, 1),
(7, 2),
(8, 1),
(8, 2),
(9, 2),
(10, 2),
(10, 8),
(11, 2),
(11, 6),
(12, 2),
(12, 6),
(12, 8),
(13, 2),
(13, 6),
(14, 6),
(15, 2),
(15, 6),
(16, 6),
(16, 8),
(17, 1),
(17, 4),
(18, 2),
(18, 8),
(19, 2),
(19, 3),
(19, 8),
(20, 2),
(20, 3),
(20, 8),
(21, 2),
(21, 3),
(21, 8),
(22, 2),
(22, 3),
(22, 8),
(23, 2),
(23, 5),
(24, 2),
(24, 5),
(25, 2),
(25, 8),
(26, 3),
(26, 4),
(26, 8),
(27, 2),
(27, 7),
(28, 2),
(28, 7),
(29, 2),
(29, 5),
(29, 9),
(30, 2),
(30, 5),
(31, 2),
(31, 5),
(31, 9),
(32, 2),
(32, 5),
(33, 2),
(33, 5),
(33, 9),
(34, 2),
(34, 5),
(35, 2),
(35, 5),
(35, 10),
(36, 4),
(36, 5),
(37, 2),
(37, 5),
(37, 10),
(38, 1),
(38, 2),
(38, 5),
(39, 2),
(39, 5),
(40, 2),
(40, 5),
(41, 4),
(41, 5),
(42, 4),
(42, 5),
(43, 5),
(44, 2),
(44, 5);

-- --------------------------------------------------------

--
-- Table structure for table `foodingr`
--

CREATE TABLE IF NOT EXISTS `foodingr` (
  `foodid` bigint(20) NOT NULL DEFAULT '0',
  `ingrid` bigint(20) NOT NULL DEFAULT '0',
  `optional` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`foodid`,`ingrid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `foodingr`
--

INSERT INTO `foodingr` (`foodid`, `ingrid`, `optional`) VALUES
(1, 1, 0),
(1, 2, 0),
(1, 3, 0),
(1, 4, 0),
(1, 5, 0),
(1, 6, 0),
(1, 9, 0),
(2, 7, 0),
(2, 8, 0),
(2, 9, 0),
(2, 10, 0),
(2, 11, 0),
(2, 12, 0),
(2, 13, 0),
(2, 14, 0),
(3, 1, 0),
(3, 3, 0),
(3, 5, 0),
(3, 15, 0),
(3, 17, 0),
(3, 18, 0),
(3, 19, 0),
(3, 20, 0),
(5, 9, 0),
(5, 21, 0),
(4, 22, 0),
(6, 22, 0),
(7, 1, 0),
(7, 15, 0),
(7, 10, 0),
(7, 4, 0),
(7, 5, 0),
(7, 23, 0),
(8, 18, 0),
(8, 17, 0),
(8, 1, 0),
(8, 15, 0),
(8, 5, 0),
(8, 23, 0),
(8, 16, 0),
(9, 24, 0),
(9, 23, 0),
(9, 17, 0),
(9, 18, 0),
(9, 3, 0),
(9, 25, 0),
(10, 26, 0),
(10, 28, 0),
(10, 12, 0),
(10, 27, 0),
(10, 29, 0),
(10, 6, 0),
(10, 9, 0),
(11, 16, 0),
(11, 27, 0),
(11, 30, 0),
(11, 31, 0),
(12, 26, 0),
(12, 32, 0),
(12, 27, 0),
(12, 34, 0),
(12, 10, 0),
(12, 16, 0),
(12, 35, 0),
(13, 16, 0),
(13, 38, 0),
(13, 36, 0),
(14, 16, 0),
(14, 28, 0),
(14, 37, 0),
(14, 12, 0),
(15, 16, 0),
(15, 38, 0),
(15, 3, 0),
(15, 4, 0),
(15, 27, 0),
(15, 12, 0),
(16, 24, 0),
(16, 23, 0),
(16, 3, 0),
(16, 12, 0),
(16, 25, 0),
(16, 39, 0),
(17, 1, 0),
(17, 15, 1),
(17, 3, 1),
(17, 20, 0),
(17, 40, 0),
(17, 37, 0),
(17, 17, 0),
(17, 18, 0),
(18, 7, 0),
(18, 6, 0),
(18, 9, 0),
(18, 11, 0),
(18, 5, 0),
(18, 4, 0),
(18, 2, 0),
(19, 7, 0),
(19, 8, 0),
(19, 9, 0),
(19, 11, 0),
(19, 12, 0),
(19, 13, 0),
(19, 14, 0),
(19, 10, 0),
(20, 7, 0),
(20, 8, 0),
(20, 9, 0),
(20, 11, 0),
(20, 12, 0),
(20, 13, 0),
(20, 14, 0),
(20, 10, 0),
(20, 27, 0),
(21, 7, 0),
(21, 8, 0),
(21, 9, 0),
(21, 11, 0),
(21, 12, 0),
(21, 13, 0),
(21, 14, 0),
(21, 28, 0),
(21, 37, 0),
(22, 7, 0),
(22, 8, 0),
(22, 9, 0),
(22, 11, 0),
(22, 12, 0),
(22, 13, 0),
(22, 14, 0),
(22, 28, 0),
(22, 41, 0),
(22, 42, 0),
(23, 43, 0),
(23, 6, 0),
(23, 9, 0),
(23, 4, 1),
(23, 5, 0),
(23, 20, 0),
(23, 46, 1),
(23, 2, 1),
(24, 43, 0),
(24, 6, 0),
(24, 9, 0),
(24, 27, 1),
(24, 44, 1),
(24, 45, 1),
(24, 47, 1),
(25, 49, 0),
(25, 48, 0),
(25, 11, 0),
(25, 13, 0),
(25, 12, 0),
(25, 9, 0),
(26, 7, 0),
(26, 11, 0),
(26, 12, 0),
(26, 13, 0),
(26, 14, 0),
(26, 40, 0),
(27, 50, 0),
(28, 50, 0),
(29, 11, 0),
(29, 12, 0),
(29, 38, 0),
(29, 4, 0),
(29, 27, 0),
(29, 51, 0),
(29, 52, 0),
(30, 53, 0),
(30, 4, 0),
(30, 12, 0),
(30, 54, 0),
(30, 55, 0),
(30, 6, 0),
(30, 41, 0),
(30, 27, 0),
(30, 56, 0),
(31, 43, 0),
(31, 57, 0),
(31, 58, 0),
(31, 59, 0),
(31, 60, 0),
(31, 61, 0),
(31, 13, 0),
(32, 53, 0),
(32, 62, 0),
(32, 63, 0),
(32, 64, 0),
(32, 65, 0),
(32, 44, 0),
(32, 66, 0),
(32, 47, 0),
(33, 62, 0),
(33, 12, 0),
(33, 13, 0),
(33, 67, 0),
(33, 68, 0),
(33, 69, 0),
(33, 59, 0),
(33, 47, 0),
(34, 43, 0),
(34, 70, 0),
(34, 72, 0),
(34, 66, 0),
(34, 44, 0),
(34, 45, 0),
(34, 6, 0),
(34, 71, 0),
(35, 43, 0),
(35, 72, 0),
(35, 78, 0),
(35, 79, 0),
(35, 60, 0),
(36, 43, 0),
(36, 63, 0),
(36, 70, 0),
(36, 66, 0),
(36, 71, 0),
(36, 44, 0),
(37, 11, 0),
(37, 83, 0),
(37, 67, 0),
(37, 13, 0),
(37, 57, 0),
(37, 60, 0),
(37, 78, 0),
(37, 79, 0),
(37, 61, 0),
(38, 53, 0),
(38, 86, 0),
(38, 12, 0),
(38, 6, 0),
(38, 87, 0),
(38, 85, 0),
(38, 58, 0),
(38, 41, 0),
(38, 46, 0),
(38, 38, 0),
(38, 20, 0),
(39, 53, 0),
(39, 12, 0),
(39, 54, 0),
(39, 76, 0),
(39, 73, 0),
(39, 74, 0),
(39, 77, 0),
(39, 75, 0),
(39, 27, 0),
(40, 53, 0),
(40, 73, 0),
(40, 74, 0),
(40, 75, 0),
(40, 6, 0),
(41, 43, 0),
(41, 72, 0),
(41, 44, 0),
(41, 55, 0),
(41, 47, 0),
(42, 53, 0),
(42, 12, 0),
(42, 80, 0),
(42, 81, 0),
(42, 82, 0),
(42, 68, 0),
(43, 43, 0),
(43, 83, 0),
(43, 13, 0),
(43, 85, 0),
(43, 84, 0),
(43, 12, 0),
(43, 67, 0),
(43, 58, 0),
(43, 46, 0),
(43, 20, 0),
(44, 53, 0),
(44, 73, 0),
(44, 74, 0),
(44, 75, 0);

-- --------------------------------------------------------

--
-- Table structure for table `foodloc`
--

CREATE TABLE IF NOT EXISTS `foodloc` (
  `foodid` bigint(20) NOT NULL DEFAULT '0',
  `locationid` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`foodid`,`locationid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `foodloc`
--

INSERT INTO `foodloc` (`foodid`, `locationid`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 2),
(30, 2),
(31, 2),
(32, 2),
(33, 2),
(34, 2),
(35, 2),
(36, 2),
(37, 2),
(38, 2),
(39, 2),
(40, 2),
(41, 2),
(42, 2),
(43, 2),
(44, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ingrcat`
--

CREATE TABLE IF NOT EXISTS `ingrcat` (
  `ingrid` bigint(20) NOT NULL DEFAULT '0',
  `ingrcatid` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ingrid`,`ingrcatid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ingrcat`
--

INSERT INTO `ingrcat` (`ingrid`, `ingrcatid`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 4),
(6, 5),
(7, 1),
(8, 5),
(9, 6),
(10, 3),
(11, 4),
(12, 4),
(13, 4),
(14, 4),
(15, 2),
(16, 7),
(17, 4),
(18, 4),
(19, 8),
(20, 4),
(21, 8),
(22, 3),
(23, 5),
(24, 1),
(25, 2),
(26, 1),
(27, 5),
(28, 3),
(29, 2),
(30, 5),
(31, 1),
(32, 5),
(33, 5),
(34, 2),
(35, 2),
(36, 5),
(37, 4),
(38, 3),
(39, 7),
(40, 9),
(41, 8),
(42, 2),
(43, 4),
(44, 10),
(45, 4),
(46, 8),
(47, 2),
(48, 5),
(49, 1),
(50, 8),
(51, 11),
(52, 2),
(53, 4),
(54, 7),
(55, 3),
(56, 2),
(57, 4),
(58, 4),
(59, 11),
(60, 9),
(61, 2),
(62, 4),
(63, 4),
(64, 4),
(65, 5),
(66, 3),
(67, 4),
(68, 3),
(69, 10),
(70, 4),
(71, 2),
(72, 4),
(73, 1),
(74, 3),
(75, 2),
(76, 5),
(77, 2),
(78, 5),
(79, 2),
(80, 4),
(81, 4),
(82, 2),
(83, 4),
(84, 6),
(85, 2),
(86, 4),
(87, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ingredient`
--

CREATE TABLE IF NOT EXISTS `ingredient` (
  `ingrid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ingrname` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`ingrname`,`ingrid`),
  UNIQUE KEY `ingrid` (`ingrid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=88 ;

--
-- Dumping data for table `ingredient`
--

INSERT INTO `ingredient` (`ingrid`, `ingrname`) VALUES
(10, 'american'),
(63, 'Apple'),
(79, 'Asian peanut'),
(4, 'avocado'),
(27, 'bacon'),
(48, 'Bacon ranch chicken '),
(47, 'Balsamic vinaigrette'),
(87, 'Barbeque'),
(8, 'beef, ground'),
(64, 'Beet'),
(20, 'black beans'),
(55, 'Bleu cheese'),
(76, 'Buffalo chicken'),
(7, 'bun, brioche'),
(9, 'burger/fry seasoning'),
(75, 'Caesar dressing'),
(67, 'Carrot'),
(84, 'Cayenne pepper'),
(38, 'Cheddar'),
(50, 'Chicken strips, brea'),
(6, 'chicken, grilled'),
(24, 'ciabatta bun'),
(58, 'Cilantro'),
(2, 'cilantro-lime ranch'),
(86, 'Corn'),
(45, 'Cranberries, dried'),
(73, 'Croutons'),
(83, 'Cucumber'),
(57, 'Edamame'),
(39, 'Egg, fried'),
(54, 'Egg, hard-boiled'),
(16, 'egg, scrambled'),
(68, 'Feta'),
(25, 'fire roasted pepper '),
(21, 'french fries'),
(66, 'Goat Cheese'),
(82, 'Greek'),
(36, 'Ham, diced'),
(33, 'Ham, shaved'),
(81, 'Heart of palm'),
(56, 'Honey Dijon'),
(29, 'honey mustard'),
(77, 'Hot sauce'),
(52, 'Jalapeno Ranch'),
(11, 'lettuce, iceberg'),
(53, 'Lettuce, Romaine'),
(43, 'Lettuce, spring mix'),
(72, 'Mandarin Oranges'),
(15, 'mayo, spicy'),
(34, 'Mayonnaise'),
(22, 'mozzarella'),
(37, 'Mushrooms'),
(80, 'Olive, Kalamata'),
(41, 'Onion straws'),
(13, 'onion, red'),
(18, 'onions, roasted'),
(31, 'pancakes'),
(74, 'Parmesan'),
(44, 'Pecans, glazed'),
(3, 'pepperjack'),
(17, 'peppers, roasted'),
(14, 'pickle'),
(5, 'pico de gallo'),
(26, 'potato bread'),
(19, 'potato rounds'),
(23, 'prime rib'),
(85, 'Ranch'),
(71, 'Raspberry dressing'),
(59, 'Salmon'),
(32, 'Sausage crumbles'),
(30, 'sausage links'),
(61, 'Sesame Ginger'),
(51, 'Shrimp'),
(62, 'Spinach'),
(42, 'Steak sauce'),
(70, 'Strawberries'),
(69, 'Sunflower seeds'),
(35, 'Sweet grilling sprea'),
(28, 'swiss'),
(78, 'Teriyaki Chicken'),
(12, 'tomato'),
(46, 'Tortilla threads'),
(1, 'tortilla, flour'),
(65, 'Turkey'),
(40, 'Veggie burger patty'),
(49, 'Whole grain bread'),
(60, 'Wonton strips');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `locationid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `location_name` varchar(100) DEFAULT NULL,
  `locaddress` varchar(100) DEFAULT NULL,
  `about` varchar(4000) NOT NULL,
  PRIMARY KEY (`locationid`),
  UNIQUE KEY `locationid` (`locationid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`locationid`, `location_name`, `locaddress`, `about`) VALUES
(1, 'The Den', '875 Traditions Way Tallahassee, FL 32306', ''),
(2, 'Salad Creations', '75 N. Woodward Ave. Tallahassee, FL 32306', ''),
(3, 'Chilis', '196 Academic Way Tallahassee, FL 32306', '');

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE IF NOT EXISTS `rate` (
  `userid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `foodid` bigint(20) unsigned NOT NULL,
  `rating` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`userid`,`foodid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `rate`
--

INSERT INTO `rate` (`userid`, `foodid`, `rating`) VALUES
(22, 1, 4),
(22, 2, 2),
(22, 5, 3),
(22, 7, 3),
(22, 4, 3),
(22, 3, 1),
(22, 6, 5),
(20, 1, 5),
(20, 2, 3),
(20, 3, 3),
(20, 4, 1),
(20, 5, 1),
(20, 6, 4),
(20, 7, 3),
(21, 1, 2),
(21, 2, 4),
(21, 3, 4),
(21, 4, 4),
(21, 5, 3),
(21, 6, 2),
(21, 7, 3),
(22, 20, 2),
(22, 35, 3);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(200) NOT NULL DEFAULT '',
  `password` varchar(20) DEFAULT NULL,
  `userid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`username`,`userid`),
  UNIQUE KEY `userid` (`userid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `password`, `userid`) VALUES
('1799664_test@%', NULL, 1),
('aparker', NULL, 2),
('', NULL, 3),
('test_user1', 'none', 4),
('bob', NULL, 5),
('test10', 'password10', 6),
('nametest', 'passwordtest', 8),
('test@yahoo.com', 'password', 9),
('jap10f@my.fsu.edu', 'I am beatiful', 15),
('jpena0095@gmail.com', 'I am ugly', 12),
('anotherTest@testing.', 'shit', 13),
('mobile.solutions.ent', 'another test', 16),
('asdf@asdf.com', 'asdfadsfas', 17),
('asdf', 'asdfasdf', 18),
('testing', 'test', 19),
('Hello@testing.com', 'pwd', 20),
('chickentest@foo.com', 'a', 21),
('bob_the_tester@test.me', 'testing', 22),
('test2@fake.com', 'asdfasdf1', 32),
('asd@asdf.com', 'asdfasdf1', 33),
('asd@asdf.com', 'asdfasdf1', 34),
('Kbrown3429@gmail.com', 'watermelon92', 35),
('c@c.com', 'asdfasdf123', 36),
('tester@tester.com', 'testing88', 37);

-- --------------------------------------------------------

--
-- Table structure for table `useraller`
--

CREATE TABLE IF NOT EXISTS `useraller` (
  `userid` bigint(20) NOT NULL DEFAULT '0',
  `ingrid` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userid`,`ingrid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `useraller`
--

INSERT INTO `useraller` (`userid`, `ingrid`) VALUES
(2, 5),
(22, 4);

-- --------------------------------------------------------

--
-- Table structure for table `userdis`
--

CREATE TABLE IF NOT EXISTS `userdis` (
  `userid` bigint(20) NOT NULL DEFAULT '0',
  `ingrid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userid`,`ingrid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userdis`
--

INSERT INTO `userdis` (`userid`, `ingrid`) VALUES
(22, 5),
(22, 12),
(22, 37);

-- --------------------------------------------------------

--
-- Table structure for table `userfavs`
--

CREATE TABLE IF NOT EXISTS `userfavs` (
  `userid` bigint(20) NOT NULL DEFAULT '0',
  `foodid` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userid`,`foodid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `userlikes`
--

CREATE TABLE IF NOT EXISTS `userlikes` (
  `userid` bigint(20) NOT NULL DEFAULT '0',
  `ingrid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userid`,`ingrid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userlikes`
--

INSERT INTO `userlikes` (`userid`, `ingrid`) VALUES
(2, 12),
(22, 10),
(22, 63),
(22, 79);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
