<? include 'accesscontrol.php'; ?>
<!DOCTYPE HTML>
<html>
<? include 'header.php'; ?>
    <div data-role="page" data-theme='b'>
	
		<? include 'menu.php' ?>
	
        <div data-role="header" data-position="fixed" data-tap-toggle="false" data-theme='b'>
            <a href="#mypanel" data-ajax="false"><i class='fa fa-bars'></i></a>
            <h5>Browse Options</h5>
        </div>
        
        <div data-role="content"> 
			<ul data-role="listview" data-inset="false" data-icon="false" data-divider-theme="b">
		  
				<li>
				<li><a href="displayCategories.php" data-ajax="false">
					<img src="./images/pyr.png">
					<h2><i class='blIcon fa fa-spoon'></i><Strong>Categories</strong></h2> 
				</a></li>
				
				<li><a href="displayIngredientsCategories.php" data-ajax="false">
				<img src="./images/ingred.png">
					<h2><i class='blIcon fa fa-cutlery'></i><Strong>Ingredients</strong></h2>
				</a></li>
				
				
			
			</ul>
	        
        </div>

    </div>
    
    </body>
</html>
