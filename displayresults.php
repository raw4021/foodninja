<? include 'accesscontrol.php'; ?>
<!DOCTYPE HTML>
<html>
<? include 'header.php'; ?>
<?php
function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
            }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}
?>
    <div data-role="page" data-theme='b'>


		<? include 'menu.php' ?>
	
        <div data-role="header" data-position="fixed" data-tap-toggle="false" data-theme='b'>
            <a href="#mypanel" data-ajax="false"><i class='fa fa-bars'></i></a>
            <h5>WELCOME</h5>
        </div>
        
        <div data-role="content">   
				<ul data-role="listview" data-inset="false" data-icon="false" data-divider-theme="b">
					<li>
					<h1>You will love them!</h1>
					<p class="ui-li-aside"></p>
					</li>
					<li data-role="list-divider"><span class="ui-li-count"></li>
						<?
					//for each food item
					$sql = "SELECT * FROM `food`";
					$result = mysql_query($sql);
					$data[]= array();
					if (mysql_num_rows($result) > 0) {
					// output data of each row into an array
					while($row = mysql_fetch_array($result) ) {
						//for each food item not in dislikes or allergies, but in likes
						$foodid = $row["foodid"];
						$InUserLikes = "ingredient.ingrname IN (SELECT ingredient.ingrname FROM `userlikes` RIGHT JOIN `ingredient` ON (userlikes.ingrid = ingredient.ingrid) WHERE userlikes.userid = '$userid')";
						$notInUserAllergies = "ingredient.ingrname NOT IN (SELECT ingredient.ingrname FROM `useraller` RIGHT JOIN `ingredient` ON (useraller.ingrid = ingredient.ingrid) WHERE useraller.userid = '$userid')";
						$notInUserDislikes = "ingredient.ingrname NOT IN (SELECT ingredient.ingrname FROM `userdis`  RIGHT JOIN `ingredient` ON (userdis.ingrid = ingredient.ingrid) WHERE userdis.userid = '$userid')";
						$bigSQL = "SELECT COUNT(*) FROM `food`, `ingredient`, `foodingr` WHERE food.foodid = '$foodid' AND food.foodid = foodingr.foodid AND foodingr.ingrid = ingredient.ingrid AND $notInUserAllergies AND $notInUserDislikes AND $InUserLikes";
						//for testing
						//echo "<li data-role='list-divider'>" . $bigSQL . "</li>";
						$countResult = mysql_query($bigSQL);
						$finalCount = mysql_fetch_array($countResult);
						//for testing only
						//echo "<li data-role='list-divider'>Foodid: " .$row["foodid"]." count: " . $finalCount["COUNT(*)"] . "</li>";
						$data[] = array('foodid' => $row["foodid"], 'Foodname' => $row["foodname"], 'hitCount' => $finalCount["COUNT(*)"], 'pay' => $row["pay"]);
					}
					//sort the array
					$sorted = array_orderby($data, 'hitCount', SORT_DESC);
					foreach($sorted as $newRow){
							if($newRow["hitCount"] > 0){
						?>
						<li><a href="DISHES_info.php?foodid=<? echo $newRow["foodid"]?>">
							<img src="./images/dish/allfood.png">
							<h2><? echo $newRow["Foodname"]?></h2>
							<p><? echo $newRow["pay"]?></p>
							<p class="ui-li-aside"><strong><? echo $newRow["hitCount"] ?> Match Score</strong></p>
						</a></li>
						<?
							}
						}
					} else {
						echo "NO food items found: Internal error.";
					}
					?>
				</ul>
				<ul data-role="listview" data-inset="false" data-icon="false" data-divider-theme="b">
					<li>
					<h1>You might try them.</h1>
					<p class="ui-li-aside"></p>
					</li>
					<li data-role="list-divider"><span class="ui-li-count"></li>
						<?
					//for each food item
					$sql = "SELECT * FROM `food`";
					$result = mysql_query($sql);
					$data[]= array();
					if (mysql_num_rows($result) > 0) {
					// output data of each row into an array
					while($row = mysql_fetch_array($result) ) {
						//for each food item not in dislikes or allergies, but in likes
						$foodid = $row["foodid"];
						$notInUserLikes = "ingredient.ingrname NOT IN (SELECT ingredient.ingrname FROM `userlikes` RIGHT JOIN `ingredient` ON (userlikes.ingrid = ingredient.ingrid) WHERE userlikes.userid = '$userid')";
						$notInUserAllergies = "ingredient.ingrname NOT IN (SELECT ingredient.ingrname FROM `useraller` RIGHT JOIN `ingredient` ON (useraller.ingrid = ingredient.ingrid) WHERE useraller.userid = '$userid')";
						$notInUserDislikes = "ingredient.ingrname NOT IN (SELECT ingredient.ingrname FROM `userdis`  RIGHT JOIN `ingredient` ON (userdis.ingrid = ingredient.ingrid) WHERE userdis.userid = '$userid')";
						$bigSQL = "SELECT COUNT(*) FROM `food`, `ingredient`, `foodingr` WHERE food.foodid = '$foodid' AND food.foodid = foodingr.foodid AND foodingr.ingrid = ingredient.ingrid AND $notInUserAllergies AND $notInUserDislikes AND $notInUserLikes";
						//for testing
						//echo "<li data-role='list-divider'>" . $bigSQL . "</li>";
						$countResult = mysql_query($bigSQL);
						$finalCount = mysql_fetch_array($countResult);
						//for testing only
						//echo "<li data-role='list-divider'>Foodid: " .$row["foodid"]." count: " . $finalCount["COUNT(*)"] . "</li>";
						$data[] = array('foodid' => $row["foodid"], 'Foodname' => $row["foodname"], 'hitCount' => $finalCount["COUNT(*)"]);
					}
					//sort the array
					$sorted = array_orderby($data, 'hitCount', SORT_DESC);
					foreach($sorted as $newRow){
							if($newRow["hitCount"] > 0){
						?>
						<li><a href="DISHES_info.php?foodid=<? echo $newRow["foodid"]?>">
							<img src="./images/dish/allfood.png">
							<h2><? echo $newRow["Foodname"]?></h2>
							<p><? echo $newRow["pay"]?></p>
							<p class="ui-li-aside"><strong><? echo $newRow["hitCount"] ?> Match Score</strong></p>
						</a></li>
						<?
							}
						}
					} else {
						echo "NO food items found: Internal error.";
					}
					?>
				</ul>
        </div>

    </div>
    
    <script src="js/nativedroid.script.js"></script>
    </body>
</html>
